var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0,
};

let marker;
let markerCircle;
let map;
let infoWindow
let currentPosition

var resultMarkers = []

async function success(pos) {
    console.log(`Your current position is (${crd.latitude}, ${crd.longitude}) with ${crd.accuracy} meter accuracy`);
};

function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
};

const getCoords = async () => {
    const pos = await new Promise((success, error) => {
        navigator.geolocation.getCurrentPosition(success, error, options);
    });

    return {
      lng: pos.coords.longitude,
      lat: pos.coords.latitude,
    };
};

document.addEventListener("DOMContentLoaded", function(event) {
    // console.log("DOM Loaded")
    const input_radius = document.getElementById('input_radius');

    input_radius.addEventListener('input', function() {
        markerCircle.setRadius(input_radius.value * 1000)
    });
});

async function initMap() {
    currentPosition = await getCoords();
    // The map, centered at your position
    console.log("creating map")
    map = new google.maps.Map(document.getElementById("map"), {
        zoom: 8,
        center: currentPosition,
    });

    // The marker, positioned at your position
    console.log("creating marker")
    marker = new google.maps.Marker({
        position: currentPosition,
        map: map,
        draggable:true,
        title:"Drag me!"
    });

    // Create an info window to share between markers.
    infoWindow = new google.maps.InfoWindow();

    // The marker, around the marker whenever it goes
    markerCircle = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        radius: input_radius.value * 1000,
    });
    markerCircle.bindTo('center', marker, 'position');

    const geocoder = new google.maps.Geocoder();
    document.getElementById("address-submit").addEventListener("click", () => {
        geocodeAddress(geocoder, map);
    });
    document.getElementById('address').addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            geocodeAddress(geocoder, map);
        }
    });
}

function geocodeAddress(geocoder, resultsMap) {
    const address = document.getElementById("address").value;
    geocoder
        .geocode({ address: address })
        .then(({ results }) => {
            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location)
            map.setZoom(8)
        })
        .catch((e) =>
            alert("Geocode was not successful for the following reason: " + e)
        );
}

function addResultMarker(lat, lng, name, markerInfo) {
    var marker = new google.maps.Marker({
        position: { lat: lat, lng: lng },
        map: map,
        draggable:false,
        icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
          },
        title:name
    });
    // Add a click listener for each marker, and set up the info window.
    marker.addListener("click", () => {
        infoWindow.close();
        infoWindow.setContent(markerInfo);
        infoWindow.open(marker.getMap(), marker);
        map.setCenter(marker.getPosition());
        });

    resultMarkers.push(marker)
}

function deleteResultMarkers() {
    for (var i = 0; i < resultMarkers.length; i++) {
        resultMarkers[i].setMap(null);
    }
    resultMarkers = [];
}