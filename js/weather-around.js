

$("#go-submit").click(function(){
    // Start loading button
    loadingButtonOn()
    deleteResultMarkers()

    $.ajax({
        url: "https://weather-around-go.herokuapp.com/weather-around",
        data: { 
            "latitude": marker.position.lat(), 
            "longitude": marker.position.lng(), 
            "radius": input_radius.value
        },
        crossDomain: true,
        cache: false,
        type: "GET",
        success: async function(response) {
            // Put markers in map
            updateMapWithMarkers(response);
            let numTableElements = await refreshTable(response);
            // console.log("updated the table with " + numTableElements + " elements");
        },
        error: function(xhr) {
            console.log(xhr)
            loadingButtonOff()
            alert("Error requesting the weather for that zone. Please make sure there is any city in the red area.")
        },
        timeout: 5000
    }).done(function() {
        loadingButtonOff()
      });;
});

async function updateMapWithMarkers(arr) {
    for (var i = 0; i < arr.length; i++) {
        markerInfo = arr[i].name + " today: <br>" + weatherFormater(
            arr[i].weather.weather[0].avgtempC,
            arr[i].weather.weather[0].maxtempC,
            arr[i].weather.weather[0].mintempC,
            arr[i].weather.weather[0].avgcloudcover,
            arr[i].weather.weather[0].maxchanceofrain,
            arr[i].weather.weather[0].mmaccumulated)
        addResultMarker(parseFloat(arr[i].lat), parseFloat(arr[i].lng), arr[i].name, markerInfo)
    }
}

async function refreshTable(arr) {
    // Empty the table
    $("#tblBody").empty();
    // Empty headers (except city and current weather)
    $("#weatherTable thead").empty();
    $("#weatherTable thead").append("<th>City</th>")
    $("#weatherTable thead").append("<th>Current condition</th>")
    // Set header with new days    
    for (var i = 0; i < arr[0].weather.weather.length; i++) {
        $("#weatherTable thead").append(`<th>${arr[0].weather.weather[i].date}</th>`);
    }

    // Fill table with this array
    for (var i = 0; i < arr.length; i++) {
        $("#weatherTable tbody").append(generateTableRow(arr[i]));
    }

    // Return the number of elements updated
    return $('#weatherTable >tbody >tr').length;
}

function generateTableRow(weatherInfo) {
    let col1 = weatherInfo.name

    let col2 = currentWeatherFormater(
        weatherInfo.weather.current_condition[0].cloudcover, 
        weatherInfo.weather.current_condition[0].precipMM, 
        weatherInfo.weather.current_condition[0].temp_C)

    let col3 = weatherFormater(
        weatherInfo.weather.weather[0].avgtempC,
        weatherInfo.weather.weather[0].maxtempC,
        weatherInfo.weather.weather[0].mintempC,
        weatherInfo.weather.weather[0].avgcloudcover,
        weatherInfo.weather.weather[0].maxchanceofrain,
        weatherInfo.weather.weather[0].mmaccumulated)
    
    let col4 = weatherFormater(
        weatherInfo.weather.weather[1].avgtempC,
        weatherInfo.weather.weather[1].maxtempC,
        weatherInfo.weather.weather[1].mintempC,
        weatherInfo.weather.weather[1].avgcloudcover,
        weatherInfo.weather.weather[1].maxchanceofrain,
        weatherInfo.weather.weather[1].mmaccumulated)

    let col5 = weatherFormater(
        weatherInfo.weather.weather[2].avgtempC,
        weatherInfo.weather.weather[2].maxtempC,
        weatherInfo.weather.weather[2].mintempC,
        weatherInfo.weather.weather[2].avgcloudcover,
        weatherInfo.weather.weather[2].maxchanceofrain,
        weatherInfo.weather.weather[2].mmaccumulated)
    
    return `
    <tr>
        <td>${col1}</td>
        <td style="text-align:center; min-width:200px">${col2}</td>
        <td style="text-align:center; min-width:200px">${col3}</td>
        <td style="text-align:center; min-width:200px">${col4}</td>
        <td style="text-align:center; min-width:200px">${col5}</td>
    </tr>
    `
}

function loadingButtonOn() {
    // Disable
    $('#go-submit').prop('disabled', true);
    // Add spinner
    $("#go-submit-spinner").prop('hidden', false);

}

function loadingButtonOff() {
    // Remove spinner
    $("#go-submit-spinner").prop('hidden', true);
    // Enable
    $('#go-submit').prop('disabled', false);
}

function weatherFormater(avgtempC, maxtempC, mintempC, avgcloudcover, maxchanceofrain, mmaccumulated) {
    return `
    <p><i class="fas fa-thermometer-half"></i> ${avgtempC} °C <br> <!-- avgtempC -->
    <i class="fas fa-arrow-up"></i> ${maxtempC} °C &nbsp; <!-- maxtempC -->
    <i class="fas fa-arrow-down"></i> ${mintempC} °C </p> <!-- mintempC -->

    <p><i class="fas fa-cloud-sun"></i> ${Math.round(avgcloudcover)} % <br> <!-- avgcloudcover -->
    <i class="fas fa-cloud-rain"></i> ${maxchanceofrain} % &nbsp; <!-- maxchanceofrain --> 
    <i class="fas fa-tint"></i> ${Math.round(mmaccumulated).toFixed(1)} mm </p> <!-- mmaccumulated --> 
    `
}

function currentWeatherFormater(cloudcover, precipMM, temp_C) {
    return `
    <i class="fas fa-thermometer-half"></i> ${temp_C} °C <br> <!-- temp_C -->
    <i class="fas fa-cloud-sun"></i> ${cloudcover} % <br> <!-- cloudcover -->
    <i class="fas fa-tint"></i> ${precipMM} mm <br> <!-- precipMM --> 
    `
}